from django.db import models
from django.contrib.postgres.fields import ArrayField


class Record(models.Model):
    matrix = ArrayField(models.IntegerField(), blank=True)

    class Meta:
        db_table = 'record'
