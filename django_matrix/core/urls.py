from .api import api
from django.urls import path


core_urls = [
    path("api/", api.urls),
]
