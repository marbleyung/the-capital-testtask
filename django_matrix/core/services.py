import httpx
from typing import List
from django.http import HttpResponseBadRequest


async def traverse_matrix(original_matrix: List[List]):
    """
    The idea behind the function is: in matrix, there are two indices, row and column, or (code alias) left and right.
    Each element has its own unique row-column index, so the idea is to store elements in hashmap, using
    their row-column index as key and the element itself as value.
    Since the algorithm has no in-place limitation, let's create new list with desired result.

    Let's iterate trough hashmap and add each element to the list, the rules are:
    Start with left = 0, right = 0, N = size of the matrix, largest = size of the matrix, smallest = 0
    while left != N - 1 -> left++
    then, same for the right, so we end with left == right == N - 1;
    then, let's decrease left, until it equal the smallest number existing as hashmap key
    then, do the same for right, until it equal the smallest number++
    after this full loop, let's set largest-- and smallest++ and repeat while hashmap has elements
    """

    hashmap = {}
    n = len(original_matrix)

    for i in range(n):
        for j in range(n):
            hashmap.setdefault(str(i) + str(j), original_matrix[i][j])

    matrix = []
    left, right = 0, 0
    smallest = 0
    largest = len(original_matrix) - 1

    matrix.append(hashmap.get('00'))
    del hashmap['00']

    while hashmap:
        if left == smallest and right == smallest + 1 and hashmap:
            smallest += 1
            largest -= 1
            if smallest == largest:
                key = str(smallest) * 2
                matrix.append(hashmap.get(key))
                del hashmap[key]

        if left < largest and right < largest and right == smallest and hashmap:
            left += 1
            key = str(left) + str(right)
            matrix.append(hashmap.get(key))
            del hashmap[key]

        if left == largest and right < largest and hashmap:
            right += 1
            key = str(left) + str(right)
            matrix.append(hashmap.get(key))
            del hashmap[key]

        if (left == right == largest) or (smallest < left < largest and right == largest) and hashmap:
            left -= 1
            key = str(left) + str(right)
            matrix.append(hashmap.get(key))
            del hashmap[key]

        if left == smallest and smallest < right <= largest and hashmap:
            right -= 1
            key = str(left) + str(right)
            matrix.append(hashmap.get(key))
            del hashmap[key]

    return matrix


async def format_matrix(matrix: List[str]):
    formatted_matrix = []
    row = []

    for i in matrix[1:]:  # start with 1: to evade first '\n'
        el = i.strip()
        if el.isdigit():
            row.append(int(el))
        else:
            formatted_matrix.append(row[:])
            row.clear()

    is_matrix_incorrect = await validate_matrix_has_correct_size(formatted_matrix)
    if is_matrix_incorrect:
        return is_matrix_incorrect
    return formatted_matrix


async def trim_matrix(matrix: str) -> List:
    trimmed_matrix = (matrix
                      .replace('-', '')
                      .replace('+', '')
                      .split('|'))
    return trimmed_matrix


async def process_matrix(matrix: str):
    trimmed_matrix = await trim_matrix(matrix)
    formatted_matrix = await format_matrix(trimmed_matrix)
    if await validate_matrix_been_formatted(formatted_matrix):
        traversed_matrix = await traverse_matrix(formatted_matrix)
        return traversed_matrix
    return formatted_matrix


async def send_request(url: str) -> str:
    async with httpx.AsyncClient() as client:
        try:
            response = await client.get(url)
            return response.text
        except Exception as e:
            raise e


async def get_matrix(url: str) -> List[int]:
    matrix = await send_request(url)
    processed_matrix = await process_matrix(matrix)
    return processed_matrix


async def validate_matrix_has_correct_size(matrix: List[List[int]]):
    n = len(matrix)
    for i in matrix:
        if len(i) != n:
            return HttpResponseBadRequest("Matrix should have a size of NxN")


async def validate_matrix_been_formatted(formatted_matrix):
    if isinstance(formatted_matrix, List):
        return True

