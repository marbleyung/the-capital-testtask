from ninja import NinjaAPI
from .services import get_matrix
from typing import List

api = NinjaAPI()


@api.post("/traverse-matrix")
async def traverse_matrix(request, url: str) -> List[int]:
    traversed_matrix = await get_matrix(url)
    return traversed_matrix
