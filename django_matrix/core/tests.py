import asyncio
from .services import traverse_matrix, get_matrix
from django.http import HttpResponseBadRequest
import os
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_matrix.settings')
settings.configure()
SOURCE_URL = "https://raw.githubusercontent.com/Real-Estate-THE-Capital/python-assignment/main/matrix.txt"
INCORRECT_SOURCE_URL = "https://raw.githubusercontent.com/marbleyung/checkbox_testtask/main/wrong_matrix.txt"


TRAVERSAL = [
    10, 50, 90, 130,
    140, 150, 160, 120,
    80, 40, 30, 20,
    60, 100, 110, 70,
]

TRAVERSAL_2 = [
    10, 30,
    40, 20
]

TRAVERSAL_3 = [
    10, 40, 70,
    80, 90, 60,
    30, 20, 50
]


def generate_matrix(n):
    matrix = [[10 * (1 + i) + (j*n * 10) for i in range(n)] for j in range(n)]
    return matrix


# Create your tests here.
def test_get_matrix():
    assert asyncio.run(get_matrix(SOURCE_URL)) == TRAVERSAL


def test_get_incorrect_matrix():
    incorrect_request = asyncio.run(get_matrix(INCORRECT_SOURCE_URL))
    assert isinstance(incorrect_request, HttpResponseBadRequest)


def test_traverse_matrix():
    matrix_2 = generate_matrix(2)
    assert asyncio.run(traverse_matrix(matrix_2)) == TRAVERSAL_2
    matrix_3 = generate_matrix(3)
    assert asyncio.run(traverse_matrix(matrix_3)) == TRAVERSAL_3
    matrix_4 = generate_matrix(4)
    assert asyncio.run(traverse_matrix(matrix_4)) == TRAVERSAL
