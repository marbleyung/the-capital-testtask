#!/bin/bash

install() {
    echo "Creating virtual environment..."
    # Create virtual environment
    python3 -m venv venv

    echo "Activating virtual environment..."
    sleep 3

    source venv/bin/activate
    sleep 3
    echo "Installing dependencies..."
    pip install -r requirements.txt
    echo "Done! Run 'start' command"
}

start() {
    source venv/bin/activate
    cd django_matrix
    echo "Starting application..."
    python -m uvicorn django_matrix.asgi:application

}

test() {
    source venv/bin/activate
    cd django_matrix/core
    echo "Running tests..."
    pytest tests.py
}

# Check the command line argument
if [ "$1" == "install" ]; then
    install
elif [ "$1" == "start" ]; then
    start
elif [ "$1" == "test" ]; then
    test
else
    echo "Usage: $0 {install|start|test}"
    exit 1
fi