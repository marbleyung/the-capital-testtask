# The Capital test task

## Working on WSL/Ubuntu

## Getting started

1. clone the repo with `git clone`
2. go to the directory of the repo `cd the-capital-testtask` 
3. use `sudo ./run.sh install` to install the app
4. use `sudo ./run.sh test` to run tests
5. use `sudo ./run.sh start` to start the server
6. go to `http://localhost:8000/api/docs`
7. use the only POST endpoint, pass the URL of the matrix to the endpoint and get the result if the matrix is correct